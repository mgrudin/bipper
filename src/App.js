import React from 'react';
import { Channel } from './Components/Channel';
import logo from './logo.svg';
import './App.css';

function App() {
  const RSS_URLS = ['https://www.freecodecamp.org/news/rss/'];

  return (
    <div className="app">
      <div className="app__container">
        <header className="app__header">
          <img src={logo} className="app__logo" alt="logo" />
          <h1 className="app__title">Bipper.io</h1>
        </header>
        {RSS_URLS.map((rssUrl, i) => (
          <Channel key={i.toString()} url={rssUrl} />
        ))}
      </div>
    </div>
  );
}

export default App;
