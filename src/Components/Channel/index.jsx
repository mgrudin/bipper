import React, { useState, useEffect } from 'react';
import { RSSList } from '../RSSList';

export const Channel = ({ url }) => {
  const [channelData, setChannelData] = useState({});

  const getChannelData = async (url) => {
    try {
      const response = await fetch(url);

      const text = await response.text();
      const data = new window.DOMParser().parseFromString(text, 'text/xml');

      setChannelData({
        title: data.querySelector('image title').innerHTML,
        url: data.querySelector('image url').innerHTML,
        link: data.querySelector('image link').innerHTML,
        items: Array.from(data.querySelectorAll('item')).map((item) => ({
          title: item
            .querySelector('title')
            .innerHTML.replace('<![CDATA[', '')
            .replace(']]>', '')
            .trim(),
          link: item.querySelector('link').innerHTML,
        })),
      });
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    getChannelData(url);
  }, [url]);

  const itemURLS = [url, url + '?paged=2', url + '?paged=3'];

  return (
    <div className="channel">
      <div className="channel__header">
        <a
          href={channelData.link}
          className="channel__header-link"
          target="_blank"
          rel="noopener noreferrer"
        >
          <img
            className="channel__icon"
            src={channelData.url}
            alt={channelData.title}
            width="32"
            height="32"
          />
          <h2 className="channel__title">{channelData.title}</h2>
        </a>
      </div>
      <div className="channel__content">
        {itemURLS.map((url, i) => (
          <RSSList key={i} url={url} />
        ))}
      </div>
    </div>
  );
};
