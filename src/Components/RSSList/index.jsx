import React, { useState, useEffect } from 'react';

export const RSSList = ({ url }) => {
  const [items, setItems] = useState([]);

  console.log(url);

  const getChannelData = async (url) => {
    try {
      const response = await fetch(url);

      const text = await response.text();
      const data = new window.DOMParser().parseFromString(text, 'text/xml');

      setItems(
        Array.from(data.querySelectorAll('item')).map((item) => ({
          title: item
            .querySelector('title')
            .innerHTML.replace('<![CDATA[', '')
            .replace(']]>', '')
            .trim(),
          link: item.querySelector('link').innerHTML,
        }))
      );
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    getChannelData(url);
  }, [url]);

  return (
    <ul className="channel__list">
      {items
        ? items.map((item, i) => (
            <li key={i} className="channel__list-item">
              <a
                className="channel__link"
                target="_blank"
                rel="noopener noreferrer"
                href={item.link}
              >
                {item.title}
              </a>
            </li>
          ))
        : null}
    </ul>
  );
};
